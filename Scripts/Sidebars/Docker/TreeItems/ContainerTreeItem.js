class ContainerTreeItem extends TreeItem {
	constructor(element) {
		super(ContainerTreeItem.#label(element));
		this.set_icons_by_conf(element);
		this.contextValue = element.State;
		this.description(element);
		this.#set_tooltip(element);
	}
	
	set_icons_by_conf(element) {
		switch (nova.config.get("docker.container.icons")) {
			case "Power Symbols":
				this.on_icon = "icons/status/power/on.png";
				this.off_icon = "icons/status/power/off.png";
				break;
			case "Aqua Traffic":
				this.on_icon = "icons/status/aqua_traffic/on.png";
				this.off_icon = "icons/status/aqua_traffic/off.png";
				break;
			default:
				this.on_icon = "icons/status/phone_toggle/on.png";
				this.off_icon = "icons/status/phone_toggle/off.png";
				break;
		}
		this.#set_icons(element);
	}
	
	#set_icons(element) {
		this.image = (element.State == 'running')
			? this.on_icon
			: this.off_icon;
	}
	
	#set_tooltip(element) {
		// ES6 templating treats indentation literal,
		// so string concatenation is used.
		try {
			this.tooltip =
				"Name:\t" + element.Names + "\n" +
				"Image:\t" + element.Image + "\n" +
				"Ports:\t" + element.Ports + "\n" +
				"Size:\t\t" + element.Size + "\n" +
				"State:\t" + element.State;
		} catch(e) {
			console.warn("Unable to generate tooltip.");
		} 
	}
	
	description(element) {
		try {
			let descriptors = [];
			if (nova.config.get("docker.container.desctext.names")) {
				descriptors.push(element.Names);
			}
			if (nova.config.get("docker.context.desctext.id")) {
				descriptors.push(element.ID);
			}
			if (nova.config.get("docker.container.desctext.runningfor")) {
				descriptors.push(element.RunningFor);
			}
			if (nova.config.get("docker.container.desctext.size")) {
				descriptors.push(element.Size);
			}
			if (nova.config.get("docker.context.desctext.status")) {
				descriptors.push(element.Status);
			}
			this.descriptiveText = descriptors.join(" ");
		} catch(e) {
			this.descriptiveText = "";
		}
	}
	
	static #label(element) {
		switch (nova.config.get("docker.container.label")) {
			case "Image":
				return element.Image;
			case "ID":
				return element.ID;
			default:
				return element.Names;
		}
	}
}


module.exports = { ContainerTreeItem };
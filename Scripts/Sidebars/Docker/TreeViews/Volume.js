const notify = require("../../../Utilities/Notify");
const { VolumeTreeItem } = require("../TreeItems/VolumeTreeItem");
const { DockerClient } = require("../interfaces/DockerClient");
const { Volume } = require("../models/Volume");

class VolumeController {
	constructor() {
		this.tree = new TreeView("volumes", {
			dataProvider: this
		});
		
		this.register_commands();
		this.register_listeners();
	}
	
	deactivate() {
		this.dispose();
	}
	
	reload() {
		this.tree.reload();
	}
	
	dispose() {
		this.tree.dispose();
	}
	
	async getChildren(element) {
		if (!element) {
			let volumes = [];
			try {
				return await DockerClient.get_volumes();
			} catch(e) {
				return volumes;
			}
		}
		else {
			return element.children;
		}
	}
	
	getParent(element) {
		return element.parent;
	}
	
	getTreeItem(element) {
		return new VolumeTreeItem(element);
	}
	
	register_commands() {
		nova.commands.register("volumes.remove", async () => {
			let selection = this.tree.selection[0];
			
			try {
				let res = await DockerClient.remove_volume(selection.name);
				if (res == undefined) {
					this.reload();
				}
			} catch(e) {
				let warning = "Unable to remove volume.";
				notify.Nudge(warning, e);
			}
		});
		
		nova.commands.register("volumes.reload", async () => {
			console.log("Reloading Volumes");
			this.reload();
			nova.commands.invoke("events.reload");
		});
		
		nova.commands.register("volumes.inspect", async () => {
			let selection = this.tree.selection.map(x => x.Name);
			
			selection.forEach(async (volume_id) => {
				nova.workspace.openNewTextDocument({
					content: await DockerClient.inspect_volume(volume_id),
					syntax: "json"
				});
			});
		});
	}
	
	register_listeners() {
	}
}

module.exports = { VolumeController }

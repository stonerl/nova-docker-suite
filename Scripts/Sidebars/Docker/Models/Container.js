class Container {
	constructor(container_json) {
		this.Names = container_json.Names;
		this.Image = container_json.Image;
		this.name = this.Image;
		this.description = this.Names;
		this.uuid = this.ID = container_json.ID;
		this.Labels = container_json.Labels;
		this.State = container_json.State;
		this.RunningFor = container_json.RunningFor;
		this.Size = container_json.Size;
		this.Status = container_json.Status;
		this.Ports = container_json.Ports;
		this.Image = container_json.Image;
		
		this.children = [];
		this.parent = null;
	}
	
	addChild(element) {
		element.parent = this;
		this.children.push(element);
	}
	
	is_running() {
		if (this.State == 'running') {
			return true;
		}
		return false;
	}
}

class Project {
	constructor(container_json) {
		this.name = Project.parse_project_from_label(container_json.Labels);
		this.uuid = this.name;
		this.config = this.path = Project.parse_config_from_label(container_json.Labels);
		this.parent = null;
		this.children = [];
	}
	
	addChild(element) {
		element.parent = this;
		this.children.push(element);
	}
	
	static parse_project_from_label(label_string) {
		try {
			return label_string.match("project\=([a-zA-Z0-9-_.]+)(,|$)")[1];
		} catch(e) {
			return "Individual Containers";
		}
	}
	
	static parse_config_from_label(label_string) {
		try {
			return label_string.match("config_files\=([/a-zA-Z0-9-_.'’~\ ]+)(,|$)")[1];
		} catch(e) {
			return undefined;
		}
	}
}

class ProjectMap extends Map {
	constructor() {
		super();
	}
	
	add_by_container_json(container_json) {
		let project = new Project(container_json);
		let container = new Container(container_json);
		if (this.get(project.name)) {
			this.get(project.name).addChild(container);
		} else {
			project.addChild(container);
			this.set(project.name, project);
		}
	}
	
	valueArray() {
		return [...this.values()];
	}
}

module.exports = { Container, ProjectMap }
class Network {
	constructor(network_json=null) {
		if (network_json) {
			this.Name = this.name = network_json.Name;
			this.uuid = this.ID = network_json.ID;
			this.Driver = network_json.Driver;
			this.Internal = network_json.Internal;
			this.Scope = network_json.Scope;
			this.Labels = network_json.Labels;
			this.IPv6 = network_json.IPv6;
			
			this.children = [];
			this.parent = null;
		}
	}
	
	addChild(element) {
		element.parent = this;
		this.children.push(element);
	}
}

module.exports = { Network }
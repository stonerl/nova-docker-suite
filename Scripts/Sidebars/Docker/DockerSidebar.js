const notify = require("../../Utilities/Notify");
const { ImageController } = require("./TreeViews/Image");
const { ContainerController } = require("./TreeViews/Container");
const { ContextController } = require("./TreeViews/Context");
const { EventController } = require("./TreeViews/Event");
const { NetworkController } = require("./TreeViews/Network");
const { VolumeController } = require("./TreeViews/Volume");

exports.DockerSidebar = class DockerSidebar {
	constructor() {
		this.register_commands();
		this.start();
	}
	
	deactivate() {
		this.stop();
	}
	
	async reload() {
		this.stop();
		this.start();
	}
	
	async start() {
		
		if (nova.config.get("docker.sidebar.conf.disabled")) {
			return;
		}
		
		if (!await DockerSidebar.verify_docker_cli()) {
			return;
		}
		
		DockerSidebar.verify_docker_running();
		
		if (this.containerController) {
			this.containerController.deactivate();
			nova.subscriptions.remove(this.containerController);
		}
		if (this.contextController) {
			this.contextController.deactivate();
			nova.subscriptions.remove(this.contextController);
		}
		if (this.imageController) {
			this.imageController.deactivate();
			nova.subscriptions.remove(this.imageController);
		}
		if (this.eventController) {
			this.eventController.deactivate();
			nova.subscriptions.remove(this.eventController);
		}
		if (this.networkController) {
			this.networkController.deactivate();
			nova.subscriptions.remove(this.networkController);
		}
		if (this.volumeController) {
			this.volumeController.deactivate();
			nova.subscriptions.remove(this.volumeController);
		}
		
		this.containerController = new ContainerController();
		nova.subscriptions.add(this.containerController);
		this.contextController = new ContextController();
		nova.subscriptions.add(this.contextController);
		this.imageController = new ImageController();
		nova.subscriptions.add(this.imageController);
		this.eventController = new EventController();
		nova.subscriptions.add(this.eventController);
		this.networkController = new NetworkController();
		nova.subscriptions.add(this.networkController);
		this.volumeController = new VolumeController();
		nova.subscriptions.add(this.volumeController);
	}
	
	stop() {
		if (this.containerController)
			this.containerController.deactivate();
			
		if (this.contextController)
			this.contextController.deactivate();
		
		if (this.imageController)	
			this.imageController.deactivate();
		
		if (this.eventController)
			this.eventController.deactivate();
		
		if (this.networkController)
			this.networkController.deactivate();
		
		if (this.volumeController)
			this.volumeController.deactivate();
	}
	
	register_commands() {
		nova.commands.register("docker_suite.preferences", () => {
			nova.openConfig();
		});
		
		nova.commands.register("docker_suite.sidebar.reload", () => {
			this.reload();
		});
		
		nova.config.onDidChange("docker.sidebar.conf.disabled", () => {
			this.reload();
		});
		
	}
	
	static async verify_docker_cli() {
		return new Promise((resolve, reject) => {
			var which_docker = new Process("/usr/bin/env", {
				args: [
					"which", "docker"
				]
			})
			which_docker.onDidExit((return_code) => {
				if (return_code != 0) {
					console.error("Docker is not installed...");
					notify.ConfigNudge(
						"Docker CLI not found.",
						"Docker CLI is required for sidebar to function."
					);
					reject(false);
				}
				resolve(true);
			})
			which_docker.start();
		});
	}
	
	static async verify_docker_running() {
		return new Promise((resolve, reject) => {
			var stderr = '';
			var docker_running = new Process("/usr/bin/env", {
				args: [
					"docker", "info"
				]
			})
			docker_running.onStderr((line) => {
				stderr += line;
			})
			docker_running.onDidExit((return_code) => {
				if (return_code != 0) {
					console.error(stderr);
					notify.ConfigNudge(
						"Cannot reach contexts Docker",
						stderr,
						"verify_docker"
					);
					reject(false);
				}
				resolve(true);
			})
			docker_running.start();
		});
	}
	
}
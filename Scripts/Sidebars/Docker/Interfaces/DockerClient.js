const { ProjectMap } = require("../Models/Container");
const { Context } = require("../Models/Context");
const { RepoMap } = require('../Models/Image');
const { Network } = require('../Models/Network');
const { Volume } = require('../Models/Volume');

// TODO: Many interfaces are similar. Parameterize and/or break apart.
class DockerClient {
	
	static event_listener(on_stdout) {
		var stderr = "";
		this.event_process = new Process("/usr/bin/env", {
			args: [
				"docker", "events",
				"--filter", "type=image",
				"--filter", "type=container",
				"--filter", "type=network",
				"--filter", "type=volume",
				"--filter", "event=create",
				"--filter", "event=destroy",
				"--filter", "event=die",
				"--filter", "event=kill",
				"--filter", "event=pause",
				"--filter", "event=rename",
				"--filter", "event=restart",
				"--filter", "event=start",
				"--filter", "event=stop",
				"--filter", "event=unpause",
				"--filter", "event=update",
				"--filter", "event=delete",
				"--filter", "event=import",
				"--filter", "event=load",
				"--filter", "event=pull",
				"--filter", "event=save",
				"--filter", "event=tag",
				"--filter", "event=untag",
				"--filter", "event=remove",
				"--filter", "event=prune",
				"--format", "{{ json . }}"
			]
		})
		this.event_process.onStdout((line) => {
			on_stdout(line);
		})
		this.event_process.onStderr((line) => {
			stderr += line;
		})
		this.event_process.onDidExit((return_code) => {
			if (return_code != 0 && return_code != 15) {
				console.log(stderr);
				console.warn("Event Listener exited unexpectedly.");
			}
		})
		this.event_process.start();
	}
	
	static async #simple_proc(proc_list) {
		return new Promise((resolve, reject) => {
			var stderr = "";
			var simple_proc = new Process("/usr/bin/env", {
				args: proc_list
			})
			simple_proc.onStderr((line) => {
				stderr += line;
			})
			simple_proc.onDidExit((return_code) => {
				if (return_code != 0) {
					console.warn(stderr);
					reject(stderr);
				}
				resolve();
			})
			simple_proc.start();
		});
	}
	
	// Container Interfaces
	static async get_containers() {
		return new Promise((resolve, reject) => {
			var project_map = new ProjectMap();
			var stderr = "";
			var fetch_version = new Process("/usr/bin/env", {
				args: [
					"docker", "container",
					"ls", "-a", "--no-trunc",
					"--format", "{{ json . }}"
				]
			})
			fetch_version.onStdout((line) => {
				project_map.add_by_container_json(JSON.parse(line));
			})
			fetch_version.onStderr((line) => {
				stderr += line;
			})
			fetch_version.onDidExit((return_code) => {
				if (return_code != 0) {
					console.warn(stderr);
					reject(null);
				}
				resolve(project_map);
			})
			fetch_version.start();
		});	
	}
	
	static async start_container(container_ids) {
		console.log("Starting container(s): " + container_ids);
		return this.#simple_proc(
			[
				"docker", "container", "start"
			].concat(container_ids)
		)
	}
	
	static async restart_container(container_ids) {
		console.log("Restarting container(s): " + container_ids);
		return this.#simple_proc(
			[
				"docker", "container", "restart"
			].concat(container_ids)
		)
	}
	
	static async stop_container(container_ids) {
		console.log("Stopping container(s): " + container_ids);
		return this.#simple_proc(
			[
				"docker", "container", "stop"
			].concat(container_ids)
		)
	}
	
	static async remove_container(container_ids) {
		console.log("Removing container(s): " + container_ids);
		return this.#simple_proc(
			[
				"docker", "container", "rm"
			].concat(container_ids)
		)
	}
	
	static async inspect_container(container_id) {
		return new Promise((resolve, reject) => {
			var stdout = "";
			var stderr = "";
			var inspect_container = new Process("/usr/bin/env", {
				args: [
					"docker", "container", "inspect"
				].concat(container_id)
			})
			inspect_container.onStdout((line) => {
				stdout += line;
			})
			inspect_container.onStderr((line) => {
				stderr += line;
			})
			inspect_container.onDidExit((return_code) => {
				if (return_code != 0) {
					console.warn(stderr);
					reject(null);
				}
				resolve(stdout);
			})
			inspect_container.start();
		});
	}
	
	// Compose Interfaces
	static async start_compose(compose_file, project_name) {
		console.log(`Starting compose: ${compose_file} as ${project_name})`);
		return this.#simple_proc(
			[
				"docker-compose",
				"--file", compose_file,
				"--project-name", project_name,
				"start"
			]
		);
	}
	
	static async stop_compose(compose_file, project_name) {
		console.log(`Stopping compose: ${compose_file} as ${project_name})`);
		return this.#simple_proc(
			[
				"docker-compose",
				"--file", compose_file,
				"--project-name", project_name,
				"stop"
			]
		);
	}
	
	static async down_compose(compose_file, project_name) {
		console.log(`Down compose: ${compose_file} as ${project_name})`);
		return this.#simple_proc(
			[
				"docker-compose",
				"--file", compose_file,
				"--project-name", project_name,
				"down"
			]
		);
	}
	
	static async restart_compose(compose_file, project_name) {
		console.log(`Restarting compose: ${compose_file} as ${project_name})`);
		return this.#simple_proc(
			[
				"docker-compose",
				"--file", compose_file,
				"--project-name", project_name,
				"restart"
			]
		);
	}
	
	// Context Interfaces
	static async get_contexts() {
		return new Promise((resolve, reject) => {
			var contexts = [];
			var stderr = "";
			var fetch_contexts = new Process("/usr/bin/env", {
				args: [
					"docker", "context",
					"ls", "--format", "{{ json . }}"
				]
			})
			fetch_contexts.onStdout((line) => {
				var context = new Context(JSON.parse(line));
				contexts.push(context);
			})
			fetch_contexts.onStderr((line) => {
				stderr += line;
			})
			fetch_contexts.onDidExit((return_code) => {
				if (return_code != 0) {
					console.warn(stderr);
					reject(null);
				}
				resolve(contexts);
			})
			fetch_contexts.start();
		});	
	}
	
	static async use_context(context_name) {
		console.log(`Setting context: ${context_name}`);
		return this.#simple_proc(
			[
				"docker", "context",
				"use", context_name
			]
		)
	}
	
	static async inspect_context(context_id) {
		return new Promise((resolve, reject) => {
			var stdout = "";
			var stderr = "";
			var inspect_context = new Process("/usr/bin/env", {
				args: [
					"docker", "context", "inspect"
				].concat(context_id)
			})
			inspect_context.onStdout((line) => {
				stdout += line;
			})
			inspect_context.onStderr((line) => {
				stderr += line;
			})
			inspect_context.onDidExit((return_code) => {
				if (return_code != 0) {
					console.warn(stderr);
					reject(null);
				}
				resolve(stdout);
			})
			inspect_context.start();
		});
	}
	
	static async remove_context(context_name) {
		console.log(`Removing context: ${context_name}`);
		return this.#simple_proc(
			[
				"docker", "context",
				"rm", context_name
			]
		)
	}
	
	// Image Interfaces
	static async get_images(filter_dangling=false) {
		return new Promise((resolve, reject) => {
			var repo_map = new RepoMap();
			var stderr = "";
			var filter = filter_dangling
				? ['--filter', 'dangling=false']
				: [];
			var fetch_images = new Process("/usr/bin/env", {
				args: [
					"docker", "image",
					"ls", "--no-trunc",
					"--format", "{{ json . }}"
				].concat(filter)
			})
			fetch_images.onStdout((line) => {
				repo_map.add_by_image_json(JSON.parse(line));
			})
			fetch_images.onStderr((line) => {
				stderr += line;
			})
			fetch_images.onDidExit((return_code) => {
				if (return_code != 0) {
					console.warn(stderr);
					reject(null);
				}
				resolve(repo_map);
			})
			fetch_images.start();
		});
	}
	
	static async run_image(image_name, image_tag) {
		let image = `${image_name}:${image_tag}`;
		console.log("Running image: " + image);
		return this.#simple_proc(
			[
				"docker", "run",
				"--rm", image
			]
		)
	}
	
	static async remove_image(image_ids) {
		console.log("Removing image(s): " + image_ids);
		return this.#simple_proc(
			[
				"docker", "image", "rm"
			].concat(image_ids)
		)
	}
	
	static async inspect_image(image_id) {
		return new Promise((resolve, reject) => {
			var stdout = "";
			var stderr = "";
			var inspect_image = new Process("/usr/bin/env", {
				args: [
					"docker", "image", "inspect"
				].concat(image_id)
			})
			inspect_image.onStdout((line) => {
				stdout += line;
			})
			inspect_image.onStderr((line) => {
				stderr += line;
			})
			inspect_image.onDidExit((return_code) => {
				if (return_code != 0) {
					console.warn(stderr);
					reject(null);
				}
				resolve(stdout);
			})
			inspect_image.start();
		});
	}
	
	// Network Interfaces
	static async get_networks() {
		return new Promise((resolve, reject) => {
			var networks = [];
			var stderr = "";
			var fetch_networks = new Process("/usr/bin/env", {
				args: [
					"docker", "network",
					"ls", "--no-trunc",
					"--format", "{{ json . }}"
				]
			})
			fetch_networks.onStdout((line) => {
				var network = new Network(JSON.parse(line));
				networks.push(network);
			})
			fetch_networks.onStderr((line) => {
				stderr += line;
			})
			fetch_networks.onDidExit((return_code) => {
				if (return_code != 0) {
					console.warn(stderr);
					reject(null);
				}
				resolve(networks);
			})
			fetch_networks.start();
		});	
	}
	
	static async remove_network(network_ids) {
		console.log("Removing network(s): " + network_ids);
		return this.#simple_proc(
			[
				"docker", "network", "rm"
			].concat(network_ids)
		)
	}
	
	static async inspect_network(container_id) {
		return new Promise((resolve, reject) => {
			var stdout = "";
			var stderr = "";
			var inspect_network = new Process("/usr/bin/env", {
				args: [
					"docker", "network", "inspect"
				].concat(container_id)
			})
			inspect_network.onStdout((line) => {
				stdout += line;
			})
			inspect_network.onStderr((line) => {
				stderr += line;
			})
			inspect_network.onDidExit((return_code) => {
				if (return_code != 0) {
					console.warn(stderr);
					reject(null);
				}
				resolve(stdout);
			})
			inspect_network.start();
		});
	}
	
	// Volume Interfaces
	static async get_volumes() {
		return new Promise((resolve, reject) => {
			var volumes = [];
			var stderr = "";
			var fetch_volumes = new Process("/usr/bin/env", {
				args: [
					"docker", "volume", "ls",
					"--format", "{{ json . }}"
				]
			})
			fetch_volumes.onStdout((line) => {
				var volume = new Volume(JSON.parse(line));
				volumes.push(volume);
			})
			fetch_volumes.onStderr((line) => {
				stderr += line;
			})
			fetch_volumes.onDidExit((return_code) => {
				if (return_code != 0) {
					console.warn(stderr);
					reject(null);
				}
				resolve(volumes);
			})
			fetch_volumes.start();
		});	
	}
	
	static async inspect_volume(container_id) {
		return new Promise((resolve, reject) => {
			var stdout = "";
			var stderr = "";
			var inspect_volume = new Process("/usr/bin/env", {
				args: [
					"docker", "volume", "inspect"
				].concat(container_id)
			})
			inspect_volume.onStdout((line) => {
				stdout += line;
			})
			inspect_volume.onStderr((line) => {
				stderr += line;
			})
			inspect_volume.onDidExit((return_code) => {
				if (return_code != 0) {
					console.warn(stderr);
					reject(null);
				}
				resolve(stdout);
			})
			inspect_volume.start();
		});
	}
	
	static async remove_volume(volume_ids) {
		console.log("Removing volume(s): " + volume_ids);
		return this.#simple_proc(
			[
				"docker", "volume", "rm"
			].concat(volume_ids)
		)
	}
}

module.exports = { DockerClient }
const ContainerShell = Object.freeze({
	ash:	{code: 2, name: "ash"},
	bash:	{code: 3, name: "bash"},
	sh:		{code: 0, name: "sh"},
	identify() {
		return `which ${
			ContainerShell.ash.name
		} && return ${
			ContainerShell.ash.code
		} || which ${
			ContainerShell.bash.name
		} && return ${
			ContainerShell.bash.code
		}`
	},
	cmd(code) {
		if (code == ContainerShell.ash.code) {
			return ContainerShell.ash.name;
		} else if (code == ContainerShell.bash.code) {
			return ContainerShell.bash.name;
		}
		return ContainerShell.sh.name;
	}
});

const TerminalApp = Object.freeze({
	terminal:		{
		name: "Terminal",
		script: `${nova.extension.path}/Scripts/AppleScripts/Terminal.applescript`
	},
	iterm:			{
		name: "iTerm2",
		script: `${nova.extension.path}/Scripts/AppleScripts/iTerm2.applescript`
	},
	stringToApp(enumString) {
		if (enumString == TerminalApp.iterm.name) {
			return TerminalApp.iterm;
		}
		return TerminalApp.terminal;
	},
	userTerminal() {
		return TerminalApp.stringToApp(
			nova.config.get('docker.sidebar.terminal')
		);
	}
});

class Terminal {

	static async shell_probe(container_id) {
		return new Promise((resolve, reject) => {
			var stderr = "";
			var simple_proc = new Process("/usr/bin/env", {
				args: [
					"docker", "exec",
					container_id, "sh", "-c",
					ContainerShell.identify()
				]
			})
			simple_proc.onStderr((line) => {
				stderr += line;
			})
			simple_proc.onDidExit((return_code) => {
				if (return_code == 1) {
					console.warn(stderr);
					reject(null);
				}
				resolve(ContainerShell.cmd(return_code));
			})
			simple_proc.start();
		});
	}
	
	static async open_terminal(container_id) {
		
		let shell = await Terminal.shell_probe(container_id);
		
		return new Promise((resolve, reject) => {
			var stderr = "";
			var open_terminal = new Process("/usr/bin/env", {
				args: [
					TerminalApp.userTerminal().script,
					`docker exec -it ${container_id} ${shell} -l`
				]
			})
			open_terminal.onStderr((line) => {
				stderr += line;
			})
			open_terminal.onDidExit((return_code) => {
				if (return_code != 0) {
					console.warn(stderr);
					reject(null);
				}
				resolve();
			})
			open_terminal.start();
		});	
	}
	
	static async tail_container(container_id) {
		return new Promise((resolve, reject) => {
			var stderr = "";
			var tail_container = new Process("/usr/bin/env", {
				args: [
					TerminalApp.userTerminal().script,
					`docker logs -f --tail 1000 ${container_id}`
				]
			})
			tail_container.onStderr((line) => {
				stderr += line;
			})
			tail_container.onDidExit((return_code) => {
				if (return_code != 0) {
					console.warn(stderr);
					reject(null);
				}
				resolve();
			})
			tail_container.start();
		});	
	}
	
}

module.exports = { Terminal }

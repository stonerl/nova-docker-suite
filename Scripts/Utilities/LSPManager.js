const { host } = require("./Host");
const notify = require("./Notify");

// When repo json is fetched, cache with repo url
// This helps avoid redundant requests for the same repo url
let _cache = {};

exports.LSPManager = class LSPManager {
	constructor(repo_url) {
		this.repo_url = repo_url;
	}
	
	get cache() {
		if (!_cache[this.repo_url]) {
			try {
				_cache[this.repo_url] = this.download_json();
			} catch(e) {
				throw e;
			}
		}
		return _cache[this.repo_url];
	}
	
	/**
	 * Determines url to the repos sha json
	 * @type {string}  - URL for repos sha json
	 */
	get json_url() {
		return nova.path.join(this.repo_url, 'main', 'binaries.json');
	}
	
	/**
	 * Determines & generates path to extension storage directory
	 * @type {string}  - Path to storage
	 */
	get storage_path() {
		let path = nova.extension.globalStoragePath;
		if (nova.fs.stat(path) == null) {
			nova.fs.mkdir(path);
		}
		return path;
	}
	
	/**
	 * Checks cache and/or downloads repo sha json & sys-arch
	 * @returns {Promise}  - Promise to return status of json & sys-arch
	 */
	async update() {
		this.arch = await host.arch;
		try {
			this.json = await this.cache	
		} catch(e) {
			throw `Unable to retrieve latest LSP binaries: ${JSON.stringify(e)}`;
		}
	}
	
	/**
	 * Async - Downloads and makes executable provided LSP by name
	 * @param {string} lsp_name - Name of LSP
	 * @returns {Promise}  - Promise to return status of download
	 */
	async install(lsp_name) {
		
		if (!await this.is_latest(lsp_name)) {
			try {
				await this.download_lsp(lsp_name);	
			} catch(e) {
				throw `Unable to download ${lsp_name}: ${JSON.stringify(e)}`;
			}
		}
		
		let lsp_path = this.install_path(lsp_name);
		LSPManager.make_exec(lsp_path);
	}
	
	/**
	 * Async - Downloads LSP binary from repo
	 * @param {string} lsp_name - Name of LSP
	 * @returns {Promise}  - Promise to return status of download
	 */
	async download_lsp(lsp_name) {
		// TODO: I don't like notifications embedded here
		notify.Nudge(
			`Fetching ${lsp_name}`,
			`Fetching lateset version of ${lsp_name}`,
			`${lsp_name}_dl`
		);
		return new Promise((resolve, reject) => {
			var response = "";
			var stderr = "";
			var curl_bin = new Process("/usr/bin/env", {
				args: [
					"curl", "--no-progress-meter",
					this.bin_url(lsp_name),
					"-o", this.install_path(lsp_name)
				]
			})
			curl_bin.onStdout((line) => {
				response += line;
			})
			curl_bin.onStderr((line) => {
				stderr += line;
			})
			curl_bin.onDidExit((return_code) => {
				if (return_code != 0) {
					reject({
						status: return_code,
						stderr: stderr.trim()
					});
				} else {
					// TODO: I don't like notifications embedded here
					notify.Nudge(
						`Updated ${lsp_name}`,
						`Successfully fetched latest ${lsp_name}`,
						`${lsp_name}_dl_s`
					);
					resolve(return_code);
				}
			})
			curl_bin.start();
		});
	}
	
	/**
	 * Async - Downloads sha json from repo
	 * @returns {Promise}  - Promise to return repos sha json
	 */
	async download_json() {
		console.log("Fetching latest repo shas.");
		return new Promise((resolve, reject) => {
			var response = "";
			var stderr = "";
			var fetch_sha = new Process("/usr/bin/env", {
				args: [
					"curl", "--no-progress-meter",
					this.json_url
				]
			})
			fetch_sha.onStdout((line) => {
				response += line;
			})
			fetch_sha.onStderr((line) => {
				stderr += line;
			})
			fetch_sha.onDidExit((return_code) => {
				if (return_code != 0) {
					reject({
							status: return_code,
							stderr: stderr.trim()
						});
				} else {
					resolve(JSON.parse(response));
				}
			})
			fetch_sha.start();
		});	
	}
	
	/**
	 * Returns URL to provided LSP from repo
	 * @param {string} lsp_name - Name of LSP
	 */
	bin_url(lsp_name) {
		return nova.path.join(this.repo_url, 'main', this.arch, lsp_name);
	}
	
	/**
	 * Determines path to installed LSP by name
	 * @param {string} lsp_name - Name of LSP
	 */
	install_path(lsp_name) {
		return nova.path.join(this.storage_path, lsp_name);
	}
	
	/**
	 * Checks if a the provided LSP name is installed
	 * @param {string} lsp_name - Name of LSP
	 * @returns {boolean}  - True if found, false otherwise
	 */
	is_installed(lsp_name) {
		if (nova.fs.stat(this.install_path(lsp_name)))
			return true;
		return false;
	}
	
	/**
	 * Async - Generates sha from installed LSP by name
	 * @param {string} lsp_name - Name of LSP
	 * @returns {Promise}  - Promise to return sha256 as string
	 */
	async installed_sha(lsp_name) {
		if (this.is_installed(lsp_name)) {
			return await LSPManager.sha(
				this.install_path(lsp_name)
			);
		}
	}
	
	/**
	 * Async - Evaluates if installed LSP is up-to-date with repo
	 * @param {string} lsp_name - Name of LSP
	 * @returns {bool}  - If latest: true, Else: false
	 */
	async is_latest(lsp_name) {
		if (this.is_installed(lsp_name)) {
			var latest_sha = this.latest_sha(lsp_name);
			var installed_sha = await this.installed_sha(lsp_name);
			return latest_sha == installed_sha;
		}
		return false;
	}
	
	/**
	 * Retrieves the LSP sha from repo
	 * @param {string} lsp_name - Name of LSP
	 */
	latest_sha(lsp_name) {
		return this.json[this.arch][lsp_name].sha256;
	}
	
	/**
	 * Makes installed LSP executable for LangClient
	 * @param {string} lsp_path - Path to installed LSP
	 */
	static make_exec(lsp_path) {
		if (!nova.fs.access(lsp_path, nova.fs.constants.X_OK)) {
			nova.fs.chmod(lsp_path, 0o755);
		}
	} 
	
	/**
	 * Async - Computes sha256 of installed LSP
	 * @param {string} lsp_path - Path to installed LSP
	 * @returns {Promise}  - Promise to return sha256 as string
	 */
	static async sha(lsp_path) {
		return new Promise((resolve, reject) => {
			var response = "";
			var stderr = "";
			var compute_sha = new Process("/usr/bin/env", {
				args: ["openssl", "sha256", "-r", lsp_path]
			})
			compute_sha.onStdout((line) => {
				response += line;
			})
			compute_sha.onStderr((line) => {
				stderr += line;
			})
			compute_sha.onDidExit((return_code) => {
				if (return_code != 0) {
					reject({
						status: return_code,
						stderr: stderr.trim()
					});
				}
				var resp_split = response.split(" ").find(e => true);
				var clean_resp = resp_split.trim();
				resolve(clean_resp);
			})
			compute_sha.start();
		});
	}
	
}
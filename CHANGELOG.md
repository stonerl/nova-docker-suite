## Version 1.3
- Updates to sidebar event listener for Networks and Volumes
- Network and Volume sidebar now require a selection for context options.

## Version 1.2.9
- Volume sidebar

## Version 1.2.8
- Short-term correction for circumstances where RUN heredocs could break parsing.

## Version 1.2.7
- Updated sidebar icon per [feedback](https://gitlab.com/joncoole/nova-docker-suite/-/issues/5)
	- Special thanks to [Bryan Buchanan](https://gitlab.com/bryanbuchanan) for his feedback and contribution

## Version 1.2.6
- More Dockerfile & nested Bash highlighting updates

## Version 1.2.5
- Several updates to Dockerfile Bash highlighting

## Version 1.2.4
- Dockerfile parsing improvements
	- Correctly recognize RUN modifiers
	- CMD JSON array highlighting
- Bash parsing improvements, notably:
	- Proper handling of switch cases
	- Improved path and shell direction
- More highlighting to accompany various improvements to come. 

## Version 1.2.3
- Added a disable option to the Docker Sidebar.
	- User requested feature to prevent notifications when Docker is not installed.
	- Setting is found under the Docker Suite Sidebar settings category.
	- Defaults to false upon first installation.

## Version 1.2.2
- Minimal Network Sub Sidebar

Menu is getting cramped and Nova still doesn't support collapsing or moving sub sidebars.
Considered allowing settings to modify the extension.json, but that doesn't perform live reloads.
Need to investigate more, or fingers crossed Nova gets a much needed update.

## Version 1.2.1
- Enabled 'Tail Logs' for stopped containers

## Version 1.2.0
#### Docker Compose Language Service (Testing)
Added experimental support for Docker Compose LS, with unfortunate Nova limitations
- Supports Compose completions, hover and validation
- Activates on YAML and newly added Compose syntax
	- Compose syntax adds nested bash syntax highlighting
- Similar to Dockerfile, portable binary option available or use your own
- Disabled by default, enable in settings

Current limitations:
- Nova does not handle multiple LS for a single syntax
	- As such, Compose LS activates on YAML & Compose syntax
- Nova *ALWAYS* prioritizes built-in syntax, namely built-in YAML
	- As such, Nova does not allow me to auto-detect Compose syntax
**Due to these API limitations**
- Nova does not allow me to detect Compose syntax over their built-in YAML
- Nova will override the Compose LS with *ANY* YAML LS
	- If you are using a YAML LS, manually select Compose syntax to bypass

## Version 1.1.9.2
- Added support for iTerm2 as sidebar terminal.
	- Select in settings.

More to come.

## Version 1.1.9.1

#### Language Service
- Refactored LS configuration to a dropdown
	- Now allows for disabling a LS, if undesired
	- If old settings don't transfer over, re-enable Dockerfile LS in settings

#### Updates to Notifications
- On a trial basis, Enable/Disable Notifications has been removed
	- Notifications are now limited to strictly essential situations

More to come.

## Version 1.1.9
- Corrected an issue where special character would cause a Containers Compose path to return empty. (i.e. iCloud Drive)

## Version 1.1.8

#### Docker Sidebar Updates
- Added "Toggle Dangling" setting for Images.
- Condensed header options to settings menu for future options.
	- Trial bases for upcoming options. Feedback desired.

#### Dockerfile LSP Updates
- Initial LSP Client & Repo refactoring to improve reuse. (compose)
- Migration to new LSP repo.
	- New Dockerfile LSP.


More to come.

## Version 1.1.7

#### Docker Sidebar Updates
- Added "Inspect" functionality for Containers and Images.
- Added "Tail Logs" functionality for running Containers.
	- Due to limitations in Nova this will open in OS X Terminal.
- New Container customization options added.
	- Choose label value: Name, Repo:Tag, ID
	- Container Status icons added.
	- Descriptive text options added.
- Added hover tooltip to Containers.
	- Additional details to be added.
- Updated some sidebar notifications to include Reload option.


More to come.

## Version 1.1.6

#### Docker Sidebar Updates
- Notifications if Docker is missing or not running. 
- User notifications for any Sidebar command issues.
- Settings shortcut added to Sidebar headers.
- Context status icon customization added.
	- New Context icons added.
	- Original icons available through settings.
- Context description customization added.
- Attach in Terminal option should now auto-focus.


More to come.

## Version 1.1.5

#### Docker Sidebar Updates
- Multi-select support added for:
	- Containers
	- Compose
	- Images


Next few updates still focusing on QoL improvements.

Compose LS to be added to QoL list, despite Nova limitations.

More to come.

## Version 1.1.4

#### Docker Sidebar Updates
- Live Container and Image updates.
	- The Docker Sidebar now triggers an event listener at launch to automatically update when changes occur to containers and images.
	- If Docker Suite is run prior to Docker/Colima, reloading contexts or using the included command will restart the listener.
	- As further views are added (e.g. Networks & Volumes) automatic reloading will be standard.
	
- Open Container session in Terminal ("Attach Terminal")
	- As Nova does not expose its built-in terminal through the API, this option will instead trigger an AppleScript to launch Terminal.app with the session exec.
	- As this is using AppleScript to launch Terminal.app, macOS will prompt for permission.
	- If you, like I, hope to see Nova add support for leveraging the built-in Nova terminal rather than AppleScript hacks, please let them know!


Next few updates will focus on various QoL improvements.

File Explorer planned post QoL.

More to come.

## Version 1.1.3

#### Bug fix for latest Docker Engine release.
- Updated Sidebar formatting request from deprecated standard to new standard.
- Major thanks to [@gabriel_rinaldi](https://gitlab.com/gabriel_rinaldi) bringing this to my [attention](https://gitlab.com/joncoole/nova-docker-suite/-/issues/1)!


Live updates are still on track to be released this weekend.

Feel free to report any further issues.

More to come.

## Version 1.1.2

#### Docker Sidebar Updates
- Retain the collapsed state of projects and images between reloads.
	- Essential for the upcoming live updates functionality, otherwise very frustrating.
- Minor context label customization. (DockerEndpoint or Name)
	- Additional customization and subtext options for all categories coming.
- Various backend changes.


Live updates to containers & images coming next update.

More features to come!

## Version 1.1.1

#### Docker Sidebar Updates
- Docker Compose Start|Stop|Down|Restart support
- Updated 'Project' name to be subtext
	- Customization planned.
- Reloading of Containers and Images upon context swap within sidebar

#### Bug Fixes
- Corrected an issue where project containers would randomly be treated as an individual container.


More to come.

## Version 1.1.0

Alpha release of the Docker Client Sidebar.
- View Contexts, Containers and Images
- Start, Stop, Remove etc...


More features to come!

## Version 1.0.4

Prepping for Docker-Compose language server support. 
Due to limitations in Nova, release will be delayed until certain updates.

Various syntax enhancements.

Corrected an issue when starting/stopping the experimental binary feature that would not reactivate the path specified ls.

## Version 1.0.3

New **experimental** feature to **automatically** fetch and configure a stand-alone binary language server. 
When enabled, this removes the Node.js dependency as well as the manual installation requirements. 
Enable it from the extension preferences (internet required to fetch the binary). Please give it a try!

An experimental workaround to better detect Dockerfiles that utilize the filename extension to define purpose.
Currently limitations exist in Nova for proper detection where the filename is static and extension is dynamic.
Current work around attempts to match content per a couple common Dockerfile keywords and syntax comment.

Added a Docker Suite reload command-let.

Various other improvements.

Special congratulations to everyone that worked on Artemis 1! 🚀

## Version 1.0.2

Added missing comment-out support.

Experimental automated Language Server inclusion coming this week.
Perusing a pre-compiled solution to omit the need for the host to offer both node and the module.

## Version 1.0.1

Optional LSP notifications, improved highlighting and minor cleanup

### Basic Notifications for LSP
- Nova Notification if the docker-langserver appears missing at path
- Nova Notification if the Language Server exits unexpectedly
- LSP Notifications can be disabled in extension settings

### Additional improvements to highlighting
- More to come

## Version 1.0

Initial release

#### Adding support to Nova for:
- Dockerfile syntax
- Bash subsyntax
- Dockerfile LSP

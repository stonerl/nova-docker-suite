(if_statement
	"if" @start
	"fi" @end.after
	(#set! scope.byLine))
	
(case_statement
	"case" @start
	"esac" @end.after
	(#set! scope.byLine))

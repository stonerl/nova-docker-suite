(block_mapping_pair
 key: 
   (flow_node) @inert (#match? @inert "(?i)^(command|entrypoint)$")
 value:
   (flow_node) @injection.content
   (#set! injection.language "shell")
 )
 
 (block_mapping_pair
  key: 
    (flow_node) @inert (#match? @inert "(?i)^(command|entrypoint)$")
  value:
    (block_node
        (block_scalar ">" @injection.content.start) @injection.content.end.after
        (#set! injection.language "shell")
    )
  )